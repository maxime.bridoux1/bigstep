INCLUDE_DIR = spec/interpreter/exec \
			  spec/interpreter/syntax \
			  spec/interpreter/util \
			  spec/interpreter/runtime \
			  spec/interpreter/text \
			  spec/interpreter/script \
			  spec/interpreter/binary

all: make

make: makeI
	ocamlbuild $(INCLUDE_DIR:%=-I %) -use-ocamlfind -r -libs bigarray out/interpreter.native

makeI:
	./main.native WASM_bigstep.txt out/out.ml

clean:
	rm -r _build
	rm out/out.ml
	rm interpreter.native
