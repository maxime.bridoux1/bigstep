BASE
number
tunop
tbinop

FLOW
state

PROGRAM
instr : state * state
instr_struct : state * state
frame_struct : state * state

- constructors
cons : instr * instr_struct -> instr_struct
block : instr_struct -> instr
frame : instr_struct * frame_struct -> frame_struct
instr_end : instr_struct
frame_end : frame_struct
const : number -> instr
cunop : tunop -> instr
cbinop : tbinop -> instr
nop : instr
unreachable : instr
if : instr_struct * instr_struct -> instr
br : number -> instr
br_if : number -> instr
return : instr
call : number -> instr 
skip : instr
loop : number * instr_struct -> instr
label : number * instr_struct * instr_struct -> instr

- filters
push : state * number -> state
pop : state -> state * number
binop : tbinop * number * number -> number
unop : tunop * number -> number
id : state -> state
trap : state -> state
isZero : number -> unit
isnotZero : number -> unit
isBranching : state -> unit
isNotBranching : state -> unit
makeBrState : state * number -> state
makeLabel : number * instr_struct * instr_struct -> instr
makeBlankLabel : instr_struct -> instr
makeLoop : number * instr_struct -> instr_struct
saveStack : state -> state
isNotBr0 : state -> unit
reduceBr : state -> state
isBr0 : state -> unit
popNValues : state * number -> state * state
restoreStack : state * number * state -> state
pushNValues : state * state -> state

- atoms
initial_state : unit -> state

- rules
Cons(cons x_t1 x_t2) = [H(x_s, x_t1, x_f); [| [isBranching(x_f); id(x_f) ?> (x_o)] || [isNotBranching(x_f); H(x_f, x_t2, x_o)] |]{x_o}]
Instr_end(instr_end) = [id(x_s) ?> (x_o)]
Frame_end(frame_end) = [id(x_s) ?> (x_o)]
Skip(skip) = [id(x_s) ?> (x_o)]
Const(const x_t1) = [push(x_s, x_t1) ?> (x_o)]
Unop(cunop x_t1) = [pop(x_s) ?> (x_f1, x_t2); unop(x_t1, x_t2) ?> (x_t3); push(x_f1, x_t3) ?> (x_o)]
Binop(cbinop x_t1) = [pop(x_s) ?> (x_f1, x_t2); pop(x_f1) ?> (x_f2, x_t3); binop(x_t1, x_t2, x_t3) ?> (x_t4); push(x_f2, x_t4) ?> (x_o)]
Nop(nop) = [id(x_s) ?> (x_o)]
Unreachable(unreachable) = [trap(x_s) ?> (x_o)]
Block(block x_t1) = [makeBlankLabel(x_t1) ?> (x_t2); H(x_s, x_t2, x_o)] 
Loop(loop x_t1 x_t2) = [makeLoop(x_t1, x_t2) ?> (x_t3); makeLabel(x_t1, x_t2, x_t3) ?> (x_t4); H(x_s, x_t4, x_o)]
Branch(br x_t1) = [makeBrState(x_s, x_t1) ?> (x_o)]
If(if x_t1 x_t2) = [pop(x_s) ?> (x_f1, x_t3); [| [isZero(x_t3); H(x_f1, x_t1, x_o)] || [isnotZero(x_t3); H(x_f1, x_t2, x_o)] |] {x_o}]
Label(label x_t1 x_t2 x_t3) = [saveStack(x_s) ?> (x_f1); H(x_s, x_t2, x_f2); [| [isNotBranching(x_f2); id(x_f2) ?> (x_o)] || [isBranching(x_f2); isNotBr0(x_f2); reduceBr(x_f2) ?> (x_o)] || [isBranching(x_f2); isBr0(x_f2); popNValues(x_f2, x_t1) ?> (x_f4, x_f3); restoreStack(x_f1, x_t1, x_f4) ?> (x_f5); pushNValues(x_f5, x_f3) ?> (x_f6); H(x_f6, x_t3, x_o)] |]{x_o}]

