open Ast
open Types
open I32

let print_ast_var (v:Ast.var) = print_string (Int32.to_string v.it);;

let rec print_instr (i:instr) = match i.it with
  | Unreachable -> print_string "Unreachable"
  | Nop -> print_string "Nop"
  | Drop -> print_string "Drop"
  | Select -> print_string "Select"
  | Block(s,i) -> print_string "Block "; print_instr_list i (*of stack_type * instr list*)
  | Loop(s,i) -> print_string "Label "; print_instr_list i (*of stack_type * instr list*)
  | If(s,i1,i2) -> print_string "If then "; print_instr_list i1; print_newline (); print_string "Else "; print_instr_list i2
  | Br(v) -> print_string "Br "; print_ast_var v                       (* break to n-th surrounding label *)
  | BrIf(v) -> print_string "BrIf "; print_ast_var v                       (* conditional break *)
  | BrTable(vs,v) -> print_string "BrTable"          (* of var list * var indexed break *)
  | Return -> print_string "Return"                           (* break from function body *)
  | Call(v) -> print_string "Call "; print_ast_var v                        (* call function *)
  | CallIndirect(v) -> print_string "CallIndirect "; print_ast_var v                 (* call function through table *)
  | LocalGet(v) -> print_string "LocalGet "; print_ast_var v                    (* read local variable *)
  | LocalSet(v) -> print_string "LocalSet "; print_ast_var v                     (* write local variable *)
  | LocalTee(v) -> print_string "LocalTee "; print_ast_var v                     (* write local variable and keep value *)
  | GlobalGet(v) -> print_string "GlobalGet "; print_ast_var v                  (* read global variable *)
  | GlobalSet(v) -> print_string "GlobalSet "; print_ast_var v                    (* write global variable *)
  | Load(loadop) -> print_string "Load"                   (* read memory at address *)
  | Store(storeop) -> print_string "Store"                 (* write memory at address *)
  | MemorySize -> print_string "MemorySize"                       (* size of linear memory *)
  | MemoryGrow -> print_string "MemoryGrow"                       (* grow linear memory *)
  | Const(literal) -> print_string "Const"                 (* constant *)
  | Test(testop) -> print_string "Test"                   (* numeric test *)
  | Compare(relop) -> print_string "Compare"                 (* numeric comparison *)
  | Unary(unop) -> print_string "Unary"                    (* unary numeric operator *)
  | Binary(binop) -> print_string "Binary"                  (* binary numeric operator *)
  | Convert(cvtop) -> print_string "Convert"                 (* conversion *)

and print_instr_list (is: instr list) = match is with
    | [] -> print_string "end"
    | h::t -> print_instr h; print_newline; print_instr_list t;;

let print_func (f:Ast.func) = print_instr_list f.it.body;;

let rec print_func_list fl = match fl with
    | [] -> print_string "fl end"
    | h::t -> print_func h; print_newline (); print_func_list t;;

let print_module (m:Ast.module_) = print_func_list m.it.funcs;;

let print_var (v:Script.var) = print_string v.it;;

let print_var_opt v = match v with
    | Some(a) -> print_var a
    | None -> print_string "None"
    ;;

let print_def (d:Script.definition) = match d.it with
    | Textual(m) -> print_string "Module"; print_newline (); print_module m
    | Encoded(s1,s2) -> print_string s1; print_string s2
    | Quoted(s1,s2) -> print_string s1; print_string s2
    ;;

let rec print_name n = match n with
    | [] -> print_string "end"
    | h::t -> print_int h; print_name t;;

let print_action a = print_string "action";;
let print_assertion a = print_string "assertion";;
let print_meta m = print_string "meta";;


let print_command (c:Script.command) = match c.it with
    | Module(v,def) -> print_var_opt v; print_def def
    | Register(name, v) -> print_name name; print_var_opt v;
    | Action(a) -> print_action a
    | Assertion(a) -> print_assertion a
    | Meta(m) -> print_meta m;;

let rec print_script s = match s with
    | [] -> ()
    | h::t -> print_command h; print_newline (); print_script t;;